<?php

add_action( 'init', 'create_custom_taxonomies_deep_purple', 0 );

if (!function_exists('create_custom_taxonomies_deep_purple')) {
    function create_custom_taxonomies_deep_purple() {

        $args = array(
            'labels'            => array(
                'name'              => _x( 'Fonctions', 'taxonomy general name', 'deeppurple' ),
                'singular_name'     => _x( 'Fonction', 'taxonomy singular name', 'deeppurple' ),
                'search_items'      => __( 'Chercher un statut', 'deeppurple' ),
                'all_items'         => __( 'Toutes les fonctions', 'deeppurple' ),
                'view_item'         => __( 'Voir la fonction', 'deeppurple' ),
                'parent_item'       => __( 'Fonction parente', 'deeppurple' ),
                'parent_item_colon' => __( 'Fonction parente:', 'deeppurple' ),
                'edit_item'         => __( 'Modifier la fonction', 'deeppurple' ),
                'update_item'       => __( 'Modifier la fonction', 'deeppurple' ),
                'add_new_item'      => __( 'Ajouter une nouvelle fonction', 'deeppurple' ),
                'new_item_name'     => __( 'Nom de la fonction', 'deeppurple' ),
                'not_found'         => __( 'Pas de fonction trouvée', 'deeppurple' ),
                'back_to_items'     => __( 'Retour', 'deeppurple' ),
                'menu_name'         => __( 'Status', 'deeppurple' ),
            ),
            'hierarchical'      => true, // true -> se comporte comme une catégorie // false  -> se comporte comme
            // une étiquette
            'public'            => true,
            'show_ui'           => true, //visible dans l'interface
            'show_admin_column' => true, //visible dans la colone de navigation dans le backoffice
            'query_var'         => true, //permet de modifier des requetes à la base de données
            'rewrite'           => array( 'slug' => 'fonction' ),
            'show_in_rest'      => true, //pour l'API
        );

        register_taxonomy('fonction', 'member', $args);


        $labels = [
            "name" => __( "Régions", "deeppurple" ),
            "singular_name" => __( "Région", "deeppurple" ),
            "menu_name" => __( "Régions", "deeppurple" ),
            "all_items" => __( "Tous les Régions", "deeppurple" ),
            "edit_item" => __( "Modifier Région", "deeppurple" ),
            "view_item" => __( "Voir Région", "deeppurple" ),
            "update_item" => __( "Mettre à jour le nom de Région", "deeppurple" ),
            "add_new_item" => __( "Ajouter un nouveau Région", "deeppurple" ),
            "new_item_name" => __( "Nom du nouveau Région", "deeppurple" ),
            "parent_item" => __( "Parent dRégion", "deeppurple" ),
            "parent_item_colon" => __( "Parent Région :", "deeppurple" ),
            "search_items" => __( "Recherche de Régions", "deeppurple" ),
            "popular_items" => __( "Régions populaires", "deeppurple" ),
            "separate_items_with_commas" => __( "Séparer les Régions avec des virgules", "deeppurple" ),
            "add_or_remove_items" => __( "Ajouter ou supprimer des Régions", "deeppurple" ),
            "choose_from_most_used" => __( "Choisir parmi les Régions les plus utilisés", "deeppurple" ),
            "not_found" => __( "Aucun Régions trouvé", "deeppurple" ),
            "no_terms" => __( "Aucun Régions", "deeppurple" ),
            "items_list_navigation" => __( "Navigation de liste de Régions", "deeppurple" ),
            "items_list" => __( "Liste de Régions", "deeppurple" ),
            "back_to_items" => __( "Retourner à Régions", "deeppurple" ),
        ];

        $args = [
            "label" => __( "Régions", "deeppurple" ),
            "labels" => $labels,
            "public" => true,
            "publicly_queryable" => true,
            "hierarchical" => false,
            "show_ui" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "query_var" => true,
            "rewrite" => [ 'slug' => 'region', 'with_front' => true, ],
            "show_admin_column" => false,
            "show_in_rest" => true,
            "rest_base" => "region",
            "rest_controller_class" => "WP_REST_Terms_Controller",
            "show_in_quick_edit" => false,
        ];
        register_taxonomy( "region", [ "member" ], $args );
    }
}

