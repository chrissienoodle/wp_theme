<?php

add_action('init', 'create_member_cpt');
function create_member_cpt(){

    //création d'un custom post type pour les membres du groupes
    $args = array(
        'labels'    => array(
            'name'                  => _x( 'Membres', 'Post type general name', 'deeppurple' ),
            'singular_name'         => _x( 'Membre', 'Post type singular name', 'deeppurple' ),
            'menu_name'             => _x( 'Membres', 'Admin Menu text', 'deeppurple' ),
            'name_admin_bar'        => _x( 'Membre', 'Add New on Toolbar', 'deeppurple' ),
            'add_new'               => __( 'Ajouter un membre', 'deeppurple' ),
            'add_new_item'          => __( 'Ajouter un nouveau membre', 'deeppurple' ),
            'new_item'              => __( 'Nouveau membre', 'deeppurple' ),
            'edit_item'             => __( 'Modifier le membre', 'deeppurple' ),
            'view_item'             => __( 'Voir le membre', 'deeppurple' ),
            'all_items'             => __( 'Tous les membres', 'deeppurple' ),
            'search_items'          => __( 'Chercher les membres', 'deepurple' ),
            'parent_item_colon'     => __( 'Membres parents:', 'deeppurple' ),
            'not_found'             => __( 'Pas de membre.', 'deeppurple' ),
            'not_found_in_trash'    => __( 'Pas de membre dans la corbeillle.', 'deeppurple' ),
            'featured_image'        => _x( 'Image du membre', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'deeppurple' ),
            'set_featured_image'    => _x( 'Appliquer l\'image au membre', 'Overrides the “Set featured image” phrase for 
        this post type. Added in 4.3', 'deeppurple' ),
            'remove_featured_image' => _x( 'Supprimer l\'image du membre', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'deeppurple' ),
            'use_featured_image'    => _x( 'Utiliser en tant qu\'image du membre', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'deeppurple' ),
            'archives'              => _x( 'Membres', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'deeppurple' ),
            'insert_into_item'      => _x( 'Ajouter au membre', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'deeppurple' ),
            'uploaded_to_this_item' => _x( 'Ajouté au membre', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'deeppurple' ),
            'filter_items_list'     => _x( 'Filtrer les membres', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'deeppurple' ),
            'items_list_navigation' => _x( 'Naviguer dans la liste des membres', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'deeppurple' ),
            'items_list'            => _x( 'Liste des membres', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'deeppurple' ),
        ),
        'public'    => true, //permet de voir le posttype dans l'interface d'admin et en front
        'hierarchical'  => false, //fait que le pst type se comporte comme un article et non une page
        'exclude_from_search'   => false, // le post type apparait dans les résultats de recherche
        'publicly_queryable'    => true, //
        'show_ui'   => true,
        'show_in_menu'  => true,
        'show_in_nav_menus' => false,
        'show_in_admin_bar' => true,
        'show_in_rest'  => true, //Requête ajax (voir avec le cours JS)
        'menu_position' => 6, // sous les articles
        'menu_icon' => 'dashicons-universal-access-alt', //picto dans l'interface d'admin
        'supports'  => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),  //les
        // éléments constituant le post
        'taxonomies'    => array('category'),
        'has_archive'   => true,

    );
    register_post_type('member', $args);

    //création d'un custom post type pour la discographie
    $args2 = array(
        'labels'    => array(
            'name'                  => _x( 'Discographie', 'Post type general name', 'deeppurple' ),
            'singular_name'         => _x( 'Disque', 'Post type singular name', 'deeppurple' ),
            'menu_name'             => _x( 'Disques', 'Admin Menu text', 'deeppurple' ),
            'name_admin_bar'        => _x( 'Discographie', 'Add New on Toolbar', 'deeppurple' ),
            'add_new'               => __( 'Ajouter un disque', 'deeppurple' ),
            'add_new_item'          => __( 'Ajouter un nouveau disque', 'deeppurple' ),
            'new_item'              => __( 'Nouveau disque', 'deeppurple' ),
            'edit_item'             => __( 'Modifier le disque', 'deeppurple' ),
            'view_item'             => __( 'Voir le disque', 'deeppurple' ),
            'all_items'             => __( 'Tous les disques', 'deeppurple' ),
            'search_items'          => __( 'Chercher les disques', 'deepurple' ),
            'parent_item_colon'     => __( 'Disques parents:', 'deeppurple' ),
            'not_found'             => __( 'Pas de disque.', 'deeppurple' ),
            'not_found_in_trash'    => __( 'Pas de disque dans la corbeillle.', 'deeppurple' ),
            'featured_image'        => _x( 'Image du disque', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'deeppurple' ),
            'set_featured_image'    => _x( 'Appliquer l\'image au disque', 'Overrides the “Set featured image” phrase for 
        this post type. Added in 4.3', 'deeppurple' ),
            'remove_featured_image' => _x( 'Supprimer l\'image du disque', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'deeppurple' ),
            'use_featured_image'    => _x( 'Utiliser en tant qu\'image du disque', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'deeppurple' ),
            'archives'              => _x( 'Discographie', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'deeppurple' ),
            'insert_into_item'      => _x( 'Ajouter au disque', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'deeppurple' ),
            'uploaded_to_this_item' => _x( 'Ajouté au disque', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'deeppurple' ),
            'filter_items_list'     => _x( 'Filtrer les disques', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'deeppurple' ),
            'items_list_navigation' => _x( 'Naviguer dans la liste des disques', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'deeppurple' ),
            'items_list'            => _x( 'Liste des disques', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'deeppurple' ),
        ),
        'public'    => true, //permet de voir le posttype dans l'interface d'admin et en front
        'hierarchical'  => false, //fait que le pst type se comporte comme un article et non une page
        'exclude_from_search'   => false, // le post type apparait dans les résultats de recherche
        'publicly_queryable'    => true, //
        'show_ui'   => true,
        'show_in_menu'  => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'show_in_rest'  => true, //Requête ajax (voir avec le cours JS)
        'menu_position' => 6, // sous les articles
        'menu_icon' => 'dashicons-album', //picto dans l'interface d'admin
        'supports'  => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),  //les
        // éléments constituant le post
        'has_archive'   => true,

    );
    register_post_type('cd', $args2);
}
