<?php

if ( !defined('ABSPATH') ) {
    exit; // Exit if accessed directly.
}

get_header();

if ( have_posts() ) { ?>
    <section>
        <?php while ( have_posts() ) { ?>
            <article style='background-image: url("<?php the_post_thumbnail_url('medium'); ?>");
                height: 200px; width: 30%; display: inline-block; background-size: cover; background-position: center'>
                <?php the_post(); ?>

                <div style='background-color: white'>
                    <h2><?php echo get_field('fname').' '.get_field('lname'); ?></h2>
                    <p><?php echo implode(', ', get_field('status')); ?></p>
                </div>
            </article>

        <?php  } ?>
    </section>
<?php }

get_footer();
