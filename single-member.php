<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

if(have_posts()){
    while(have_posts()){
        the_post();

        ?>
        <div style='height: 250px; width: 100%;
                background-image: url("<?php the_post_thumbnail_url('full'); ?>")'>

        </div>
        <h1><?php echo get_field('fname'). ' '. get_field('lname'); ?></h1>
        <div>
            <h2
                <?php if(get_field('is_active')):
                    if(get_field('is_active') == true):
                        echo "class='blue-title'";
                    endif;
                endif;
                ?>
            >
                Biographie de <?php echo get_field('fname'). ' '. get_field('lname'); ?>
            </h2>
            <?php the_field('bio'); ?>
        </div>
<!--        --><?php //$statii = get_field('status');
//        if($statii): ?>
<!--        <ul>-->
<!--        --><?php //foreach ($statii as $status):
//            echo '<li>'.$status.'</li>';
//        endforeach; ?>
<!--        </ul>-->
<!--        --><?php //endif; ?>

        <?php $status = get_field('status');
            echo implode(', ', $status);
        ?>

        <?php if(get_field('birthdate')): ?>
            <p>Date de naissance: <?php the_field('birthdate'); ?></p>
        <?php endif; ?>
<?php

        the_content();

    }
}

get_footer();
