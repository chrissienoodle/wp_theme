<?php
if ( ! defined( 'ABSPATH' ) ) {
exit; // Exit if accessed directly.
}

get_header();

if(have_posts()){
while(have_posts()){
the_post();

?>
<div style='height: 250px; width: 100%;
    background-image: url("<?php the_post_thumbnail_url('full'); ?>")'>

        <h1><?php the_title(); ?></h1>
</div>
    <?php

    the_content();

    the_field('date');

    if(get_field('tracks')):
        the_field('tracks');
    endif;

}
}

get_footer();


