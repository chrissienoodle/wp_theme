<?php

if ( !defined('ABSPATH') ) {
    exit; // Exit if accessed directly.
}

get_header();

$args = array(
    'post_type' => 'member',
    'posts_per_page' => 2,
    'orderby' => 'rand',
);
$query = new WP_Query($args);

if($query->have_posts()): ?>
<section>
    <h2>Les 2 membres de Deep Purple que j'aime le plus</h2>
    <?php while ($query->have_posts()):
        $query->the_post(); ?>
        <article>
            <h3><?php echo get_field('fname'). ' '. get_field('lname');?></h3>
            <?php the_excerpt(); ?>
        </article>
    <?php endwhile; ?>
</section>
<?php endif;
wp_reset_postdata();

$args = array(
    'post_type' => 'cd',
    'posts_per_page' => 3,
    'orderby' => 'rand',
);
$query = new WP_Query($args);

if($query->have_posts()): ?>
    <section>
        <h2>Les 3 cds de Deep Purple que j'aime le plus</h2>
        <?php while ($query->have_posts()):
            $query->the_post(); ?>
            <article>
                <h3><?php the_title();?></h3>
                <?php the_excerpt(); ?>

            </article>
        <?php endwhile; ?>
    </section>
<?php endif;
wp_reset_postdata();

get_footer();

