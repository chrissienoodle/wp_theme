<?php

add_action('wp_enqueue_scripts', 'enqueue_deeppurple_style');
function enqueue_deeppurple_style(){
    wp_enqueue_style('deeppurple_christine_style',
    get_stylesheet_directory_uri().'/style.css',
    array('hello-elementor'),
    wp_get_theme()->get('Version')
    );
}

require 'inc/cpt.php';
require 'inc/cf.php';
require 'inc/taxonomy.php';

add_action( 'init', 'register_deep_purple_menus' );
if(!function_exists('register_deep_purple_menus')) {
    function register_deep_purple_menus() {
        register_nav_menus(
            array(
                'header-menu' => __('Header Menu'),
                'region-menu' => __('Region Menu')
            )
        );
    }
}

